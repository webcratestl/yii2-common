<?php

namespace stephencozart\yii2\helpers;


use yii\base\InvalidConfigException;
use yii\helpers\BaseStringHelper;

class StringHelper extends BaseStringHelper {

	public static function generateRandomString()
	{
		try {
			$token = \Yii::$app->getSecurity()->generateRandomString();
		} catch (InvalidConfigException $e) {
			$token = md5(uniqid(rand(), true));
		}
		return $token;
	}

}